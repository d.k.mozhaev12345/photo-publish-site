import {DOMElementsRemover} from "../pages/AuthorProfilePage";

export class InputField extends HTMLElement {
    constructor() {
        super();
        this.#setLabel();
        this.#setField();
        this.setAttribute('class', 'input_with_label')
    }

    #setLabel() {
        let label = document.createElement('label');
        label.setAttribute('for', this.getAttribute('id') + '_field');
        label.textContent = this.getAttribute('text');
        label.setAttribute('class', 'data_input_label')
        this.appendChild(label);
    }

    #setField() {
        let field = document.createElement('input');
        field.setAttribute('type', this.getAttribute('type'));
        field.setAttribute('name', 'login');
        field.setAttribute('id', this.getAttribute('id') + '_field');
        field.setAttribute('class', 'data_input_field');
        this.appendChild(field);

    }

    field() {
        return this.querySelector('.data_input_field');
    }

    invalidateField(message) {
        this.validateField();
        let text = document.createElement('div');
        text.setAttribute('class', 'invalid_field_message');
        text.textContent = message;
        this.lastElementChild.setCustomValidity(message);
        this.appendChild(text);
    }

    validateField() {
        let remover = new DOMElementsRemover()
        let elements = this.querySelectorAll('.invalid_field_message');
        for (let element of elements) {
            remover.remove(element);
        }
        remover.execute();
        this.lastElementChild.setCustomValidity('');
    }
}