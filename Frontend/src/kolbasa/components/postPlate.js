import mainTemplate from "../../templates/main.html";
import {apiUrl} from "../pages/config";

export class PostPlate extends HTMLElement {
    constructor() {
        super();
    }

    connectedCallback() {
        debugger
        this.setAttribute('class', 'post_plate')
        this.generatePost(this.getAttribute('image_src'),
            this.getAttribute('header'),
            this.getAttribute('text'),
            this.getAttribute('author'));

        this.subscribeResizeEvent()
    }


    generatePost(image_url, header, text, author) {
        let img_url = this.generateImage(image_url);
        this.innerHTML = mainTemplate.replaceAll("{post_header}", header)
            .replaceAll("{post_text}", text)
            .replaceAll("{post_author_name}", author);
        this.appendChild(img_url);

    }

    generateImage(img_urll) {
        let img_url = new Image();
        img_url.classList.add("post-image");
        img_url.onload = () => this.resizePost();
        img_url.src = apiUrl + '/images/' + img_urll;
        return img_url;
    }

    resizePost() {
        if (window.innerWidth <= 1200)
            return;
        let content = this.firstElementChild;
        let header = this.firstElementChild;
        let image = this.lastElementChild;
        if (!content || !image) return;
        debugger
        let height = image.offsetHeight;
        let width = image.offsetWidth;
        let ratio = width / height;
        let newWidth = this.offsetWidth * 0.6;
        let newHeight = 600;
        let newHeightByNewWidth = height * (newWidth/width);
        let newWidthByNewHeight = width * (newHeight/height);
        if(newHeightByNewWidth <= newHeight) {
            image.style.height = newHeightByNewWidth + 'px';
            image.style.width = newWidth + 'px';
        }
        else if(newWidthByNewHeight <= newWidth) {
            image.style.height = newHeight + 'px';
            image.style.width = newWidthByNewHeight + 'px';
        }
        this.style.maxHeight = this.lastElementChild.offsetHeight + "px";
        content.style.maxWidth = `calc(100% - ${this.lastElementChild.offsetWidth})`;
        console.log(content.style.maxHeight);
    }

    subscribeResizeEvent = () => {
        $(window).on('resize', this.resizePost);
    }

    unsubscribeResizeEvent = () => {
        $(window).off('resize', this.resizePost);
    }
}