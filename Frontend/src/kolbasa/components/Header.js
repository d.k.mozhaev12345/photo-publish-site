export class Header extends HTMLElement {
    connectedCallback() {
        let replacer = document.createElement('div');
        replacer.setAttribute('class', 'dummyShit');
        replacer.style.display = 'flex';
        this.style.position = 'fixed'
        replacer.style.height = this.#getFullHeight() + 'px';
        this.parentNode.insertBefore(replacer, this.nextSibling);
        window.addEventListener('scroll', this.onScrollEvent);
    }

    lastScroll = 0;

    onScrollEvent = () => {
        const headerReplacer = document.querySelector('.dummyShit');
        const currentScroll = $(window).scrollTop();
        let height = this.#getFullHeight()
        headerReplacer.style.height = this.#getFullHeight() + 'px';
        let top = parseInt(this.style.top) || 0;
        let g = currentScroll - this.lastScroll;
        let s = top - g;
        let n = currentScroll > this.lastScroll
            ? Math.max(-height, Math.min(parseInt(this.style.top) || 0, s))
            : Math.min(0, Math.max(parseInt(this.style.top) || 0, s));
        this.style.top = n + 'px';
        this.lastScroll = currentScroll;
    }

    #getFullHeight() {
        /*
                return this.offsetTop + this.style.marginTop || 0 + this.style.marginBottom || 0;
        */
        return $('#' + this.id).outerHeight(true);
    }
}