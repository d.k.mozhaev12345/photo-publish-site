import {getCookie} from './common.js';
import {ApplicationInterfaceManager} from "./pages/applicationInterfaceManager";
import {InputField} from "./components/inputField.js"
import {PostPlate} from "./components/postPlate";
import {DragAndDropImageForm} from "./components/DragAndDropImageForm";
import {Header} from "./components/Header";

document.addEventListener("DOMContentLoaded", async function (event) {
    customElements.define('post-plate', PostPlate)
    customElements.define('input-field', InputField)
    customElements.define('drag-and-drop-image-form', DragAndDropImageForm)
    customElements.define('app-header', Header)
    await ApplicationInterfaceManager.initApplication();
    await ApplicationInterfaceManager.loadPageFromUrl();
});
