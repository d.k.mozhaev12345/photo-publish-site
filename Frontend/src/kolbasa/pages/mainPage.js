import mainTemplate from "../../templates/main.html";
import {PostPage, PostPageArgsDto} from "./postPage";
import {Page} from "./page";
import {ApplicationInterfaceManager} from "./applicationInterfaceManager";
import {apiUrl} from "./config";
import {getCookie} from "../common";
import {LoginPage} from "./login";
import {AuthorProfileArguments, AuthorProfilePage} from "./AuthorProfilePage"

export class MainPage extends Page {
    constructor() {
        super();
        this.name = 'MainPage';
        this.onLoadFunctions = [this.initContentPlate, this.loadItems, this.subscribeScrollEvent];
        this.onUnloadFunctions = [this.clearContent, this.unsubscribeScrollEvent];
        this.onLoadFunctions.push(() => ApplicationInterfaceManager.removeMainPageButton())
    }

    _inCallback = false;
    currentPack = 0;

    loadItems = async () => {
        if (this._inCallback || this.currentPack <= -1) {
            return;
        }
        this._inCallback = true;
        this.currentPack++;
        let response = getCookie("accessToken")
            ? await fetch(`${apiUrl}/publications-preview-list/${this.currentPack}`,
                {headers: {"Authorization": "Bearer " + getCookie("accessToken")}})
            : await fetch(`${apiUrl}/publications-preview-list/${this.currentPack}`);
        if (response.ok) {
            let posts = await response.json();
            for (let postJson of posts) {
                let postElement = document.createElement('post-plate');
                this.assignAttributes(postElement, postJson);
                postElement.addEventListener('click', async (clickEvent) => {
                    if ($(clickEvent.target).is("post-plate") || $(clickEvent.target).is("div"))
                        await this.loadPostViewPage(postJson.id)
                });
                postElement.addEventListener('click', async (e) => {
                    const senderElement = e.target;
                    // Check if sender is the <div> element e.g.
                    if ($(e.target).is("a")) {
                        await ApplicationInterfaceManager
                            .loadPage(() => new AuthorProfilePage(new AuthorProfileArguments(postJson.authorLogin)));
                    }
                });
                if (postJson.header !== '\n') {
                    document.getElementById('mainFrame').appendChild(postElement);
                } else {
                    this.currentPack = -1;
                }
                this._inCallback = false;
            }

        } else {
            alert("Ошибка HTTP: " + response.status);
        }
    }

    assignAttributes(postElement, postJson) {
        postElement.setAttribute('id', postJson.id);
        postElement.setAttribute('image_src', postJson.preview);
        postElement.setAttribute('header', postJson.header);
        postElement.setAttribute('text', postJson.text);
        postElement.setAttribute('author', postJson.authorProfileName);
    }

    subscribeScrollEvent = () => {
        $(window).on('scroll', this.scrollEvent);
    }

    unsubscribeScrollEvent = () => {
        $(window).off('scroll', this.scrollEvent);
    }

    scrollEvent = async () => {
        if ($(window).scrollTop() * 1.1 >= $(document).height() - $(window).height()) {
            await this.loadItems();
        }
    }

    clearContent = () => {
        let clone = []
        for (let el of document.querySelector(".content").childNodes) {
            clone.push(el)
        }

        for (let el of clone) {
            el.remove();
        }
        this._inCallback = false;
        this.currentPack = 0;
    }

    async loadPostViewPage(postId) {
        await ApplicationInterfaceManager.loadPage(() => new PostPage(new PostPageArgsDto(postId)));
    }
}
