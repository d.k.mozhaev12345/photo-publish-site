import loginTemplate from "../../templates/login.html";
import {PostPage} from "./postPage";
import {Page} from "./page";
import {ApplicationInterfaceManager} from "./applicationInterfaceManager";
import {apiUrl} from "./config";
import {setCookie} from "../common";
import {MainPage} from "./mainPage";

export class LoginPage extends Page {
    constructor() {
        super();
        this.name = 'LoginPage';
        this.onLoadFunctions = [this.initContentPlate, this.loadItems, () => ApplicationInterfaceManager.hideButton('Логин')];
        this.onUnloadFunctions = [this.clearContent];
    }

    loadItems = async () => {
        let template = document.createElement('template');
        template.innerHTML = loginTemplate;
        template = template.content;
        template.getElementById('js-login').addEventListener('click',
            () => this.login(document.getElementById("Login").value, document.getElementById("Password").value));

        document.getElementById('mainFrame').appendChild(template);

    }

    clearContent = () => {
        let clone = []
        for (let el of document.querySelector(".content").childNodes) {
            clone.push(el)
        }

        for (let el of clone) {
            el.remove();
        }
    }

    login(login, password) {
        const dataToSend = JSON.stringify({"login": login, "password": password});
        let dataReceived = "";
        fetch(apiUrl + "/login", {
            method: "post",
            headers: {"Content-Type": "application/json"},
            body: dataToSend
        })
            .then(async resp => {
                if (resp.status === 200) {
                    dataReceived = JSON.parse(await resp.text())

                    setCookie("accessToken", dataReceived["accessToken"])
                    setCookie("refreshToken", dataReceived["refreshToken"])
                    setCookie("login", dataReceived["login"])
                    ApplicationInterfaceManager.isAuthorized = true;
                    await ApplicationInterfaceManager.loadPage(() => new MainPage());
                } else {
                    console.log("Status: " + resp.status)
                    return Promise.reject("server")
                }
            })
            .catch(err => {
                if (err === "server") return
                console.log(err)
            })

        console.log(`Received: ${dataReceived}`)
    }
}