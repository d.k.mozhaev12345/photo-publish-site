import {Page} from "./page";
import {ApplicationInterfaceManager} from "./applicationInterfaceManager";
import registrationStepOne from "../../templates/registration_login_password_step.html";
import registrationStepTwo from "../../templates/registration_personal_data_step.html";
import registrationStepThree from "../../templates/registration_profile_photo_template.html";
import {MainPage} from "./mainPage";
import {apiUrl} from "./config";
import {setCookie} from "../common";
import {DOMElementsRemover} from "./AuthorProfilePage";
import {LoginPage} from "./login";

export class RegistrationPage extends Page {
    constructor() {
        super();
        this.name = 'RegistrationPage';
        this.onLoadFunctions = [this.initContentPlate, this.loadItems, () => ApplicationInterfaceManager.hideButton('регистрация')];
        this.onUnloadFunctions = [this.clearContent];
        this.registration = {};
    }

    loadItems = async () => {
        let template = document.createElement('template');
        template.innerHTML = registrationStepOne;
        template = template.content;
        template.getElementById('next-button').addEventListener('click',
            async () => {
                await this.completeFirstStep();
            });
        debugger;
        document.getElementById('content').appendChild(template);
        document.getElementById('Repeat_Password').field().addEventListener('blur', (event) => this.validatePassword(event));
    }
    validatePassword = async () => {
        let password = document.getElementById('Password').field().value;
        let repeatPassword = document.getElementById('Repeat_Password').field().value;
        if (password !== repeatPassword) {
            document.getElementById('Repeat_Password').invalidateField('Passwords are not equal');
        } else {
            document.getElementById('Repeat_Password').validateField();
        }
        return password === repeatPassword;
    }
    completeFirstStep = async () => {
        let profileNameField = document.getElementById('Profile_name').field().value;
        let loginField = document.getElementById('Login').field().value;
        let passwordField = document.getElementById('Password').field().value;
        this.registration.profileName = profileNameField;
        this.registration.login = loginField;
        this.registration.password = passwordField;

        if (await this.checkFirstStepFieldsAndUpdateValidity()) {
            await this.loadSecondStep();
        }
    }

    async checkFirstStepFieldsAndUpdateValidity() {
        let password = await this.validatePassword();
        let profileName = this.#checkEmptyField('Profile_name');
        let login = this.#checkEmptyField('Login');
        let passwordNotEmpty = this.#checkEmptyField('Password');
        let loginTaken = await this.#checkLogin('Login');
        return password && profileName && login && passwordNotEmpty && loginTaken;
    }

    #checkEmptyField = (field) => {
        field = document.getElementById(field)
        if (!field.field().value) {
            field.invalidateField('This field could not be empty');
        } else {
            field.validateField();
        }
        return !!field.field().value;
    }
    #checkLogin = async (loginField) => {
        loginField = document.getElementById(loginField);
        let response = await fetch(apiUrl + '/users/' + loginField.field().value);
        if (response.ok) {
            loginField.invalidateField('This name already taken');
        } else if (loginField.field().validationMessage === 'This name already taken') {
            loginField.validateField()
        }
        return !response.ok;
    }


    loadSecondStep = async () => {
        this.clearContent();
        let template = document.createElement('template');
        template.innerHTML = registrationStepTwo;
        template = template.content;
        template.getElementById('next-button').addEventListener('click',
            async () => {
                await this.completeSecondStep();
            });

        document.getElementById('content').appendChild(template);
    }
    completeSecondStep = async () => {
        this.registration.email = document.getElementById('Email').field().value;
        this.registration.name = document.getElementById('Name').field().value;
        this.registration.surName = document.getElementById('Surname').field().value;
        if (this.#checkEmptyField('Email')) {
            await this.loadThirdStep();
        }
    }
    loadThirdStep = async () => {
        this.clearContent();
        let template = document.createElement('template');
        template.innerHTML = registrationStepThree;
        template = template.content;
        template.getElementById('complete-registration').addEventListener('click',
            async () => {
                await this.completeThirdStep()
                if (await this.register()) {
                    await ApplicationInterfaceManager.loadPage(() => new LoginPage());
                } else {
                    //ToDo сделать сообщение об ошибке
                    alert("//ToDo сделать сообщение об ошибке");
                }
            }
        );

        document.getElementById('content').appendChild(template);
    }
    completeThirdStep = async () => {
        this.registration.photo = document.getElementById('Photo').images[0];
    }

    clearContent = () => {
        let clone = []
        for (let el of document.querySelector(".content").childNodes) {
            clone.push(el)
        }

        for (let el of clone) {
            el.remove();
        }
    }

    #convertToFormData(object) {
        let data = new FormData();
        for (let i in object) {
            data.append(i, object[i]);
        }
        return data;
    }

    register = async () => {
        let response = await fetch(apiUrl + "/register", {
            method: "post",
            body: this.#convertToFormData(this.registration)
        });
        return response.ok;
    }
}