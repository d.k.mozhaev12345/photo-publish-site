import {Page} from "./page";
import {apiUrl} from "./config";
import mainTemplate from "../../templates/authorProfileCard.html";
import dataField from "../../templates/authorInfoField.html";
import {objectToQueryParams} from "../common";

export class AuthorProfileArguments {
    constructor(authorLogin) {
        this.authorId = authorLogin;
    }
}

export class AuthorProfilePage extends Page {
    constructor(authorProfileArguments) {
        super();
        this.authorId = authorProfileArguments.authorId;
        this.onLoadFunctions.push(async () => await this.loadItems())
        this.onUnloadFunctions.push(() => this.clearContent())
        this.name = "AuthorPage?" + objectToQueryParams({authorId: this.authorId});
    }

    //author info model
    /*"login": "string",
    "email": "string",
    "profileName": "string",
    "extraFields": {
        "name": "string",
        "surname": "string",
        "photo": nullable*/
    #unwrap

    #flattenObject(obj, delimiter = '.', prefix = '') {
        const result = {};
        for (const prop in obj) {
            if (Object.prototype.hasOwnProperty.call(obj, prop)) {
                const key = prefix ? prefix + delimiter + prop : prop;
                const value = obj[prop];
                if (typeof value === 'object' && value !== null) {
                    Object.assign(result, this.#flattenObject(value, key));
                } else {
                    result[key] = value;
                }
            }
        }
        return result;
    }

    loadItems = async () => {
        let authorInfo = await fetch(`${apiUrl}/users/${this.authorId}`);
        authorInfo = await authorInfo.json();
        authorInfo = this.#flattenObject(authorInfo);
        authorInfo['photo'] = apiUrl + '/images/' + authorInfo['photo'];
        let content = mainTemplate;
        let fillableFields = content.matchAll('{.*}');
        debugger
        for (let fillableField of fillableFields) {
            content = content.replaceAll(fillableField[0], authorInfo[fillableField[0].slice(1, -1)] || 'undefined');
        }
        let template = document.createElement('template');
        template.innerHTML = content;
        template = template.content;

        let fields = template.querySelectorAll(".author-info-field");
        let remover = new DOMElementsRemover();
        debugger;
        for (let field of fields) {
            if (field.lastElementChild.textContent === 'undefined')
                remover.remove(field);
        }
        remover.execute();

        let authorPlate = template.querySelector('.author_card_plate');
        let authorPlateContent = authorPlate.firstElementChild;

        document.querySelector('.content').appendChild(template);
    }
    //ToDo remove duplicate
    clearContent = () => {
        let clone = []
        for (let el of document.querySelector(".content").childNodes) {
            clone.push(el)
        }

        for (let el of clone) {
            el.remove();
        }
    }
}

export class DOMElementsRemover {
    #toRemove = [];

    remove(element) {
        this.#toRemove.push(element);
    }

    execute() {
        for (let element of this.#toRemove) {
            element.remove();
        }
    }
}