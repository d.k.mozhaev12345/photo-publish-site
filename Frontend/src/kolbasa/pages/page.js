import {baseContainer} from "../../templates/elements";

export class Page {
    constructor() {
        this.name = ''
        this.onLoadFunctions = [];
        this.onUnloadFunctions = [];
        this.loadFrame = async function () {
            for (let fn of this.onLoadFunctions) {
                await fn();
            }
        }

        this.unloadFrame = function () {
            for (let fn of this.onUnloadFunctions) {
                fn();
            }
        }
    }

    initContentPlate() {
        $('div#loading').hide();
        $("#content").append(baseContainer);
    }

    clearContent = () => {
        let clone = []
        for (let el of document.querySelector(".content").childNodes) {
            clone.push(el)
        }

        for (let el of clone) {
            el.remove();
        }
    }

}