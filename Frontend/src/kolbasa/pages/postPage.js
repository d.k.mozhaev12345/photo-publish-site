import mainTemplate from "../../templates/publication_page_template.html";
import {Page} from "./page";
import {ApplicationInterfaceManager} from "./applicationInterfaceManager";
import {apiUrl} from "./config";
import {getCookie, objectToQueryParams} from "../common";

export class PostPageArgsDto {
    constructor(postId) {
        this.postId = postId;
    }
}

export class PostPage extends Page {
    constructor(postPageArgsDto) {
        super();
        this.name = 'Post?' + objectToQueryParams({postId: postPageArgsDto.postId});
        this.postId = postPageArgsDto.postId;
        this.onLoadFunctions.push(this.loadPost);
        this.onLoadFunctions.push(this.initContentPlate);
        this.onUnloadFunctions = [this.clearContent];
        //ToDo clear content
    }


    loadPost = async (node, child) => {
        let response = await fetch(`${apiUrl}/publications/${this.postId}`,
            {headers: {"Authorization": "Bearer " + getCookie("accessToken")}});
        if (response.ok) {
            debugger
            let post = await response.json();
            let post_header = post["header"];
            let post_text = post["text"];
            let postId = post["id"];
            let img_url = apiUrl + '/images/' + post["images"][0];
            debugger
            let content = mainTemplate
                .replaceAll("{post_header}", post_header)
                .replaceAll("{post_text}", post_text.replaceAll('\n', '<br>'))

            let template = document.createElement('template');
            template.innerHTML = content;
            template = template.content;
            let plate = template.querySelector('.full-post-plate');
            debugger;
            for (let image of post.images) {
                let newImage = document.createElement('img');
                newImage.setAttribute('class', 'full-post-image');
                newImage.setAttribute('src', apiUrl + '/images/' + image);
                plate.appendChild(newImage);
            }
            if (post_header !== '\n') {
                document.getElementById('content').appendChild(template);
            } else {
                this.currentPack = -1;
            }
            this._inCallback = false;
        } else {
            alert("Ошибка HTTP: " + response.status);
        }
    }
}