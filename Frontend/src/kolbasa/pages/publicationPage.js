import publishTemplate from "../../templates/publish.html";
import {Page} from "./page";
import {ApplicationInterfaceManager} from "./applicationInterfaceManager";
import {apiUrl} from "./config";
import {getCookie} from "../common";


export class PublicationPage extends Page {
    constructor() {
        super();
        this.name = 'PublicationPage';
        this.onLoadFunctions = [this.loadPublish];
        this.onUnloadFunctions = [this.clearContent]
        this.onLoadFunctions.push(() => ApplicationInterfaceManager.removePublicationButton());
    }

    loadPublish = () => {
        let response = publishTemplate;
        $("#content").append(response);

        let preview = document.getElementById('publication_preview');

        let publicationPhotos = document.getElementById('publication_photos')
        document.getElementById("submit_publication")
            .addEventListener("click", () => this.uploadForm(preview.images[0], publicationPhotos.images));

    }

    clearContent() {
        let clone = []
        for (let el of document.querySelector(".content").childNodes) {
            clone.push(el)
        }

        for (let el of clone) {
            el.remove();
        }
    }

    uploadForm(previewFile, publicationPhotos) {
        const headerInput = document.querySelector('input.header_input_field');
        const textInput = document.querySelector('textarea.post_text_input_field');
        const formData = new FormData();
        formData.append('header', headerInput.value);
        formData.append('text', textInput.value);
        formData.append('previewFile', previewFile)
        for (let i = 0; i < publicationPhotos.length; i++) {
            formData.append('files', publicationPhotos[i])
        }

        fetch(apiUrl + '/publish', {
            method: 'POST',
            body: formData,
            headers: {"Authorization": "Bearer " + getCookie("accessToken")}
        })
            .then(response => {
                console.log('Form submission successful:', response);
                ApplicationInterfaceManager.loadPage(() => new PublicationPage());
            })
            .catch(error => {
                console.error('Error submitting form:', error);
            });
    }
}