import {MainPage} from "./mainPage";
import {PublicationPage} from "./publicationPage";
import {PostPage} from "./postPage";
import {deleteCookie, getCookie, queryParamsToObject, setCookie} from "../common";
import {LoginPage} from "./login";
import {apiUrl} from "./config";
import {AuthorProfilePage} from "./AuthorProfilePage";
import {RegistrationPage} from "./registration_page";

export class ApplicationInterfaceManager {
    static __PAGES__ = {
        MainPage: () => new MainPage(),
        PublicationPage: () => new PublicationPage(),
        Post: (args) => new PostPage(args),
        LoginPage: () => new LoginPage(),
        AuthorPage: (args) => new AuthorProfilePage(args),
        RegistrationPage: () => new RegistrationPage(),
    };
    static currentPage;
    static isAuthorized = false;
    static currentUserLogin = undefined;
    static defaultConditionDelegates = [ApplicationInterfaceManager.addMainPageButton];
    static authorizedConditionDelegates = [ApplicationInterfaceManager.addPublicationPageButton, ApplicationInterfaceManager.addLogoutButton];
    static unauthorizedConditionDelegates = [/*ApplicationInterfaceManager.addLoginButton, ApplicationInterfaceManager.addRegistrationButton*/];

    static async loadBasePage(currentPageNameWithParams) {
        let splitted = currentPageNameWithParams.split('?');
        let pageName = splitted[0];
        await this.loadPage(() => this.__PAGES__[currentPageNameWithParams
            ? currentPageNameWithParams.split('?')[0]
            : 'MainPage'](currentPageNameWithParams
            ? queryParamsToObject(currentPageNameWithParams.split('?')[1])
            : undefined));
    }

    static async initApplication() {
        ApplicationInterfaceManager.isAuthorized = await ApplicationInterfaceManager.checkAuth();
        ApplicationInterfaceManager.addSearch();
        window.addEventListener('popstate', async () =>
            await ApplicationInterfaceManager.loadPageFromUrl());
    }

    static async addRegistrationButton() {
        ApplicationInterfaceManager.addButton('регистрация', async () => {
            await ApplicationInterfaceManager.loadPage(ApplicationInterfaceManager.__PAGES__.RegistrationPage)
        })
    }

    static async checkAuth() {
        let refreshToken = getCookie('refreshToken');
        if (!refreshToken)
            return false;
        let response = await fetch(apiUrl + "/refresh-token?" + new URLSearchParams({
            refreshToken: encodeURI(refreshToken)
        }), {
            method: "post",
            headers: {"Authorization": "Bearer " + getCookie("accessToken")}
        });
        if (response.ok) {
            let dataReceived = JSON.parse(await response.text())
            setCookie("accessToken", dataReceived["accessToken"])
            setCookie("refreshToken", dataReceived["refreshToken"])
            setCookie("login", dataReceived["login"])
        }

        return response.ok;
    }

    static async loadPage(pageConstructor) {
        if (this.currentPage) {
            this.currentPage.unloadFrame();
        }
        await this.restoreDefaultCondition();
        this.currentPage = pageConstructor();
        window.history.pushState('data', 'title', this.currentPage.name);
        this.currentPage.loadFrame();
    }


    static async restoreDefaultCondition() {
        let headerElements = document.querySelector('.header').childNodes;
        let headerElementCopy = [];
        for (let element of headerElements) {
            headerElementCopy.push(element);
        }
        for (let element of headerElementCopy) {
            if (element.id !== 'search')
                element.remove();
        }
        for (let delegate of this.defaultConditionDelegates.concat(ApplicationInterfaceManager.isAuthorized ? this.authorizedConditionDelegates :
            this.unauthorizedConditionDelegates)) {
            delegate();
        }
    }

    static addMainPageButton() {
        //ToDo вынести вёрстку
        let button = document.createElement('div');
        button.setAttribute('class', 'header_button')
        button.setAttribute('id', 'mainPageButton')
        button.addEventListener('click', async () => {
            await ApplicationInterfaceManager.loadPage(() => new MainPage())
        })
        button.textContent = 'лента';
        document.querySelector('.header').appendChild(button);
    }

    static removeMainPageButton() {
        this.#removeElementById('mainPageButton');
    }

    static removePublicationButton() {
        this.#removeElementById('publishPageButton');
    }

    static removeLogoutButton() {
        this.#removeElementById('logoutButton');
    }

    static removeSearch() {
        this.#removeElementById('search');
    }

    static removeLoginButton() {
        this.#removeElementById('loginButton');
    }

    static addPublicationPageButton() {
        //ToDo вынести вёрстку
        let button = document.createElement('div');
        button.setAttribute('class', 'header_button');
        button.setAttribute('id', 'publishPageButton');
        button.addEventListener('click', async () => {
            ApplicationInterfaceManager.isAuthorized = await ApplicationInterfaceManager.checkAuth();
            if (ApplicationInterfaceManager.isAuthorized)
                await ApplicationInterfaceManager.loadPage(() => new PublicationPage());
            else
                await ApplicationInterfaceManager.loadPage(() => new LoginPage());
        })
        button.textContent = 'Публикация';
        document.querySelector('.header').appendChild(button);

    }

    static addLogoutButton() {

        ApplicationInterfaceManager.addButton("Выйти", () => {
            logout();
            ApplicationInterfaceManager.isAuthorized = false;
            ApplicationInterfaceManager.loadPage(() => new MainPage());
        }, "logoutButton")

        function logout() {
            const cookies = ["accessToken", "login", "refreshToken"]
            for (const cookie of cookies)
                deleteCookie(cookie);
        }
    }


    static addSearch() {
        //ToDo вынести вёрстку
        let search = document.createElement('div');
        search.setAttribute('class', 'search')
        search.setAttribute('id', 'search')
        search.textContent = 'type for search'
        document.querySelector('.header').insertBefore(search, document.querySelector('.header').firstChild);
    }

    static addButton(label, event, id = label) {
        //ToDo вынести вёрстку
        let button = document.createElement('div');
        button.setAttribute('class', 'header_button');
        button.setAttribute('id', id);
        button.addEventListener('click', event);
        button.textContent = label;
        let header = document.querySelector('.header');
        header.appendChild(button);
    }

    static hideButton(buttonLabel) {
        //ToDo вынести вёрстку
        let button = document.getElementById(buttonLabel);
        button.remove();
    }


    static #removeElementById(id) {
        document.getElementById(id).remove();
    }

    static async loadPageFromUrl() {
        const pathWithQueryString = window.location.href.split('/').at(-1);
        if (pathWithQueryString.length > 1) {
            await ApplicationInterfaceManager.loadBasePage(pathWithQueryString);
        } else {
            await ApplicationInterfaceManager.loadBasePage('MainPage');
        }
    }

    static addLoginButton() {
        ApplicationInterfaceManager.addButton("Логин", async () => {
            await ApplicationInterfaceManager.loadPage(() => new LoginPage())
        });
    }
}