import {getCookie} from "./common.js";


function addAcceptCookie() {
    document.cookie = "useCookie=True; path=/; expires=Tue, 19 Jan 2999 03:14:07 GM";
    hideCookieNotify();
}

function hideCookieNotify() {
    document.getElementById("useCookieNotify").style.visibility = "hidden";
}

document.addEventListener("DOMContentLoaded", function (event) {
    console.log(getCookie("useCookie"))

    if (getCookie("useCookie") === undefined) {
        document.getElementById("useCookieNotify").style.visibility = "visible";
    }

    document.getElementById("js-accept-cookies")
        .addEventListener('click', () => {
            addAcceptCookie();
        });

    document.getElementById("js-reject-cookies")
        .addEventListener('click', () => {
            hideCookieNotify();
        });
});