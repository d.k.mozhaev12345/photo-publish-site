const webpack = require('webpack');
const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const WriteFilePlugin = require('write-file-webpack-plugin');

module.exports = {
    entry: {
        vendor: ['./src/kolbasa/blackJackHtmlRender.js', './src/kolbasa/index.js']
    },
    module: {
        rules: [
            {
                test: /\.(js)$/,
                use: 'babel-loader'
            },
            {
                test: /\.html$/i,
                loader: "html-loader",
            }
        ],
    },
    output: {
        path: path.join(__dirname, 'public'),
        filename: 'index_bundle.js'
    },
    plugins: [
        new CopyWebpackPlugin({
                patterns: [
                    {
                        from: 'src/css',
                        to: 'css',
                        force: true,
                    },
                    {
                        from: 'src/lib',
                        to: 'lib',
                        force: true,
                    },
                    {
                        from: 'src/templates',
                        to: 'templates',
                        force: true,
                    },
                    {
                        from: "src/index.html",
                        to: "index.html",
                    }
                ]
            }
        ),
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
        }),
    ],
    mode: 'none',
    devServer:
        {
            static: {
                directory: path.join(__dirname, 'public'),
            },
            historyApiFallback: true,
            open: true,
            compress: true,
            hot: true,
            port: 9000,
            devMiddleware: {
                writeToDisk: true,
            }
        },
}