using System.Text.Json.Serialization;

namespace PhotoProject.Infrastructure.Auth;

public static class JwtTokenConfig
{
    [JsonPropertyName("secret")]
    public static string Secret { get; set; } = "gBITFOYBN&dfjffdhfgs&NOyudgsfiuh8we7yfo3847f3x";

    [JsonPropertyName("issuer")] public static string Issuer { get; set; } = "q";

    [JsonPropertyName("audience")] public static string Audience { get; set; } = "w";

    [JsonPropertyName("accessTokenExpiration")]
    public static int AccessTokenExpiration { get; set; } = 30; //minutes

    [JsonPropertyName("refreshTokenExpiration")]
    public static int RefreshTokenExpiration { get; set; } = 60 * 24; //minutes
}