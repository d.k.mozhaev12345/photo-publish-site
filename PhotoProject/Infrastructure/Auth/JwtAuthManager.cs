﻿using System.Collections.Concurrent;
using System.Collections.Immutable;
using System.IdentityModel.Tokens.Jwt;
using System.Security;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Text.Json.Serialization;
using Infrastructure.EFCore.DIExtension;
using Microsoft.IdentityModel.Tokens;
using PhotoProject.Infrastructure.DAL.Repositories;

namespace PhotoProject.Infrastructure.Auth;

public class JwtAuthManager : IJwtAuthManager
{
    private readonly Func<UserStorage> _userStorage;

    public IImmutableDictionary<string, RefreshToken> UsersRefreshTokensReadOnlyDictionary =>
        _usersRefreshTokens.ToImmutableDictionary();


    private readonly ConcurrentDictionary<string, RefreshToken>
        _usersRefreshTokens; // can store in a database or a distributed cache

    private readonly byte[] _secret;

    public JwtAuthManager(Func<UserStorage> userStorage)
    {
        _userStorage = userStorage;
        _usersRefreshTokens = new ConcurrentDictionary<string, RefreshToken>();
        _secret = Encoding.ASCII.GetBytes(JwtTokenConfig.Secret);
    }

    // optional: clean up expired refresh tokens
    public void RemoveExpiredRefreshTokens(DateTime now)
    {
        var expiredTokens = _usersRefreshTokens.Where(x => x.Value.ExpireAt < now).ToList();
        foreach (var expiredToken in expiredTokens) _usersRefreshTokens.TryRemove(expiredToken.Key, out _);
    }

    // can be more specific to ip, user agent, device name, etc.
    public void RemoveRefreshTokenByUserName(string userName)
    {
        var refreshTokens = _usersRefreshTokens.Where(x => x.Value.Login == userName).ToList();
        foreach (var refreshToken in refreshTokens) _usersRefreshTokens.TryRemove(refreshToken.Key, out _);
    }

    public JwtAuthResult GenerateTokens(string username, Claim[] claims, DateTime now)
    {
        var shouldAddAudienceClaim =
            string.IsNullOrWhiteSpace(claims.FirstOrDefault(x => x.Type == JwtRegisteredClaimNames.Aud)?.Value);

        var jwtToken = new JwtSecurityToken(
            JwtTokenConfig.Issuer,
            shouldAddAudienceClaim ? JwtTokenConfig.Audience : string.Empty,
            claims,
            expires: now.AddMinutes(JwtTokenConfig.AccessTokenExpiration),
            signingCredentials: new SigningCredentials(new SymmetricSecurityKey(_secret),
                SecurityAlgorithms.HmacSha256Signature));
        var accessToken = new JwtSecurityTokenHandler().WriteToken(jwtToken);

        var refreshToken = new RefreshToken
        {
            Login = username,
            TokenString = GenerateRefreshTokenString(),
            ExpireAt = now.AddMinutes(JwtTokenConfig.RefreshTokenExpiration)
        };
        _usersRefreshTokens.AddOrUpdate(refreshToken.TokenString, refreshToken, (_, _) => refreshToken);

        return new JwtAuthResult
        {
            AccessToken = accessToken,
            RefreshToken = refreshToken
        };
    }

    public async Task<JwtAuthResult> Refresh(string refreshToken, string accessToken, DateTime now)
    {
        if (string.IsNullOrWhiteSpace(accessToken)) throw new SecurityTokenException("Invalid token");

        if (!_usersRefreshTokens.TryGetValue(refreshToken, out var existingRefreshToken))
            throw new SecurityTokenException("Invalid token");

        var login = _usersRefreshTokens[refreshToken].Login;
        if (existingRefreshToken.Login != login || existingRefreshToken.ExpireAt < now)
            throw new SecurityTokenException("Invalid token");

        var user = await _userStorage().GetByLogin(login);
        if (user == default)
            throw new SecurityException($"User with login: {login} not found");


        var claims = new List<Claim>
        {
            new(ClaimTypes.Name, login),
            new(ClaimTypes.NameIdentifier, user.Id.ToString())
        };
        var roles = new List<Claim>();

        return GenerateTokens(login, claims.ToArray(), now); // need to recover the original claims
    }

    private static string GenerateRefreshTokenString()
    {
        var randomNumber = new byte[32];
        using var randomNumberGenerator = RandomNumberGenerator.Create();
        randomNumberGenerator.GetBytes(randomNumber);
        return Convert.ToBase64String(randomNumber);
    }
}

public class JwtAuthResult
{
    [JsonPropertyName("accessToken")] public string AccessToken { get; set; } = null!;

    [JsonPropertyName("refreshToken")] public RefreshToken RefreshToken { get; set; } = null!;
}

public class RefreshToken
{
    [JsonPropertyName("login")] public string Login { get; set; } = null!; // can be used for usage tracking
    // can optionally include other metadata, such as user agent, ip address, device name, and so on

    [JsonPropertyName("tokenString")] public string TokenString { get; set; } = null!;

    [JsonPropertyName("expireAt")] public DateTime ExpireAt { get; set; }
}