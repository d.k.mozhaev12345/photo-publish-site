﻿using Infrastructure.EFCore.Storage;
using Microsoft.EntityFrameworkCore;
using PhotoProject.Infrastructure.DAL.Models;

namespace PhotoProject.Infrastructure.DAL.Repositories;

/// <summary>
/// Фотосессии
/// </summary>
public class PhotoSessionStorage : StorageWithGuid<PhotoSessionDbo, ApplicationContext>
{
    /// <summary>
    /// Фотосессии
    /// </summary>
    /// <param name="context"></param>
    public PhotoSessionStorage(ApplicationContext context) : base(context)
    {
        Console.WriteLine("Создали PhotoSessionStorage " + context.GetHashCode());
    }


    /// <summary>
    /// Получить фотосессию по Id
    /// </summary>
    /// <param name="id">Id фотосессии</param>
    /// <returns></returns>
    public async Task<PhotoSessionDbo?> GetPhotoSession(Guid id)
    {
        return await Context.PhotoSessions.Include(x => x.UserDbo).SingleOrDefaultAsync(x => x.Id == id);
    }

    /// <summary>
    /// Получить все фотосессии по Id юзера
    /// </summary>
    /// <param name="userId">Id юзера</param>
    /// <returns></returns>
    public async Task<IEnumerable<PhotoSessionDbo>> GetUserPhotoSessions(Guid userId)
    {
        return await Context.PhotoSessions.Include(x => x.UserDbo).Where(x => x.UserDboId == userId).ToListAsync();
    }
    
    public async Task<IEnumerable<PhotoSessionDbo>> GetUserOpenPhotoSessions(Guid userId)
    {
        return await Context.PhotoSessions.Include(x => x.UserDbo).Where(x => x.UserDboId == userId && x.Status == Status.Open).ToListAsync();
    }
    
    public async Task<IEnumerable<PhotoSessionDbo>> GetUserClosedhotoSessions(Guid userId)
    {
        return await Context.PhotoSessions.Include(x => x.UserDbo).Where(x => x.UserDboId == userId && x.Status == Status.Closed).ToListAsync();
    }

    public async Task<IEnumerable<PhotoSessionDbo>> GetAllPhotoSessions()
    {
        return await Context.PhotoSessions.Include(x => x.UserDbo).ToListAsync();
    }
    
    public async Task<IEnumerable<PhotoSessionDbo>> GetOpenPhotoSessions()
    {
        return await Context.PhotoSessions.Include(x => x.UserDbo).Where(x => x.Status == Status.Open).ToListAsync();
    }
    
    public async Task<IEnumerable<PhotoSessionDbo>> GetClosedPhotoSessions()
    {
        return await Context.PhotoSessions.Include(x => x.UserDbo).Where(x => x.Status == Status.Closed).ToListAsync();
    }

    /// <summary>
    /// Получить пагинированный список фотосессий
    /// </summary>
    /// <param name="pageNumber">Номер страницы</param>
    /// <returns></returns>
    public async Task<IEnumerable<PhotoSessionDbo>> GetPhotoSessionsByPage(int pageNumber)
    {
        var count = await Context.PhotoSessions.CountAsync();
        if (pageNumber < 1 || pageNumber > count / 5 + 1)
            return Array.Empty<PhotoSessionDbo>();

        return await Context.PhotoSessions.Include(x => x.UserDbo).Skip(count - pageNumber * 5).Take(5).ToArrayAsync();
        // return await Context.PhotoSessions.Include(x => x.UserDbo).Skip(pageNumber * 5 - 5).Take(5).ToArrayAsync();
    }
}