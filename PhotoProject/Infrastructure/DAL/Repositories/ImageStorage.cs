﻿using Infrastructure.EFCore.Storage;
using PhotoProject.Infrastructure.DAL.Models;

namespace PhotoProject.Infrastructure.DAL.Repositories;

/// <summary>
/// Картинки
/// </summary>
public class ImageStorage : StorageWithGuid<ImageDbo, ApplicationContext>
{
    /// <summary>
    /// Картинки
    /// </summary>
    /// <param name="context"></param>
    public ImageStorage(ApplicationContext context) : base(context)
    {
        Console.WriteLine("Создали ImageStorage " + context.GetHashCode());
    }
}