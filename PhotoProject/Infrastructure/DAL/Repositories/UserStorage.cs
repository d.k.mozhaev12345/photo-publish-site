﻿using Infrastructure.EFCore.Storage;
using Microsoft.EntityFrameworkCore;
using PhotoProject.Infrastructure.DAL.Models;

namespace PhotoProject.Infrastructure.DAL.Repositories;

/// <summary>
/// Пользователи
/// </summary>
public class UserStorage : StorageWithGuid<UserDbo, ApplicationContext>
{
    /// <summary>
    /// Пользователи
    /// </summary>
    /// <param name="context"></param>
    public UserStorage(ApplicationContext context) : base(context)
    {
        Console.WriteLine("Создали UserStorage " + context.GetHashCode());
    }

    /// <summary>
    /// Получить информацию о пользователе по логину
    /// </summary>
    /// <param name="login">Логин пользователя</param>
    /// <returns></returns>
    public async Task<UserDbo?> GetByLogin(string login)
    {
        return await Context.Users
                            .FirstOrDefaultAsync(x => x.Login == login);
    }

    /// <summary>
    /// Получить все публикации по Id пользователя
    /// </summary>
    /// <param name="id">Id пользователя</param>
    /// <returns></returns>
    public async Task<ICollection<PublicationDbo>> GetUserPublications(Guid id)
    {
        return (await Context.Users.Include(x => x.PublicationDbos).SingleAsync(x => x.Id == id)).PublicationDbos;
    }
}