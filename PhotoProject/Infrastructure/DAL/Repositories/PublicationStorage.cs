﻿using Infrastructure.EFCore.Storage;
using Microsoft.EntityFrameworkCore;
using PhotoProject.Infrastructure.DAL.Models;

namespace PhotoProject.Infrastructure.DAL.Repositories;

/// <summary>
/// Публикации
/// </summary>
public class PublicationStorage : StorageWithGuid<PublicationDbo, ApplicationContext>
{
    /// <summary>
    /// Публикации
    /// </summary>
    /// <param name="context"></param>
    public PublicationStorage(ApplicationContext context) : base(context)
    {
        Console.WriteLine("Создали PublStorage " + context.GetHashCode());
    }


    /// <summary>
    /// Получить публикацию по Id
    /// </summary>
    /// <param name="id">Id публикации</param>
    /// <returns></returns>
    public async Task<PublicationDbo?> GetPublication(Guid id)
    {
        return await Context.Publications.Include(x => x.UserDbo).SingleOrDefaultAsync(x => x.Id == id);
    }

    /// <summary>
    /// Получить все публикации по Id юзера
    /// </summary>
    /// <param name="userId">Id юзера</param>
    /// <returns></returns>
    public async Task<IEnumerable<PublicationDbo>> GetUserPublications(Guid userId)
    {
        return await Context.Publications.Include(x => x.UserDbo).Where(x => x.UserDboId == userId).ToListAsync();
    }

    /// <summary>
    /// Получить пагинированный список публикаций
    /// </summary>
    /// <param name="pageNumber">Номер страницы</param>
    /// <returns></returns>
    public async Task<IEnumerable<PublicationDbo>> GetPublicationsByPage(int pageNumber)
    {
        var count = await Context.Publications.CountAsync();
        if (pageNumber < 1 || pageNumber > count / 5 + 1)
            return Array.Empty<PublicationDbo>();

        return await Context.Publications.Include(x => x.UserDbo).Skip(count - pageNumber * 5).Take(5).ToArrayAsync();
        // return await Context.Publications.Include(x => x.UserDbo).Skip(pageNumber * 5 - 5).Take(5).ToArrayAsync();
    }
}