﻿using Infrastructure.EFCore.Storage;
using Microsoft.EntityFrameworkCore;
using PhotoProject.Infrastructure.DAL.Models;

namespace PhotoProject.Infrastructure.DAL.Repositories;

/// <summary>
/// Записи на фотосессию
/// </summary>
public class RecordStorage : StorageWithGuid<RecordDbo, ApplicationContext>
{
    /// <summary>
    /// Записи на фотосессию
    /// </summary>
    /// <param name="context"></param>
    public RecordStorage(ApplicationContext context) : base(context)
    {
        Console.WriteLine("Создали PublStorage " + context.GetHashCode());
    }


    /// <summary>
    /// Получить запись на фотосессию по Id
    /// </summary>
    /// <param name="id">Id публикации</param>
    /// <returns></returns>
    public async Task<RecordDbo?> GetRecord(Guid id)
    {
        return await Context.Records.SingleOrDefaultAsync(x => x.Id == id);
    }
    
    public Task DeleteAllExceptOne(Guid photoSessionId, Guid id)
    {
        var recordsToRemove = Context.Records.Where(x => x.PhotoSessionDboId == photoSessionId && x.Id != id);
        Context.Records.RemoveRange(recordsToRemove);
        return Task.CompletedTask;
    }
    
    public async Task<IEnumerable<RecordDbo>> GetRecordByPhotoSession(Guid photoSessionId)
    {
        return await Context.Records.Where(x => x.PhotoSessionDboId == photoSessionId).ToListAsync();
    }

    // /// <summary>
    // /// Получить все записи на фотоссессию по Id юзера
    // /// </summary>
    // /// <param name="userId">Id юзера</param>
    // /// <returns></returns>
    // public async Task<IEnumerable<RecordDbo>> GetUserRecords(Guid userId)
    // {
    //     return await Context.Records.Include(x => x.UserDbo).Where(x => x.UserDboId == userId).ToListAsync();
    // }

    /// <summary>
    /// Получить пагинированный список записей на фотосессию
    /// </summary>
    /// <param name="pageNumber">Номер страницы</param>
    /// <returns></returns>
    public async Task<IEnumerable<RecordDbo>> GetRecordsByPage(int pageNumber)
    {
        var count = await Context.Records.CountAsync();
        if (pageNumber < 1 || pageNumber > count / 5 + 1)
            return Array.Empty<RecordDbo>();

        return await Context.Records.Include(x => x.PhotoSessionDbo).Skip(count - pageNumber * 5).Take(5).ToArrayAsync();
        // return await Context.Publications.Include(x => x.UserDbo).Skip(pageNumber * 5 - 5).Take(5).ToArrayAsync();
    }
}