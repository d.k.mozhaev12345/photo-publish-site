﻿using System.Text.Json;
using Microsoft.EntityFrameworkCore;
using PhotoProject.Infrastructure.DAL.Models;

namespace PhotoProject.Infrastructure.DAL;

public class ApplicationContext : DbContext
{
    public DbSet<PublicationDbo> Publications => Set<PublicationDbo>();
    public DbSet<ImageDbo> Images => Set<ImageDbo>();
    public DbSet<UserDbo> Users => Set<UserDbo>();
    public DbSet<PhotoSessionDbo> PhotoSessions => Set<PhotoSessionDbo>();
    public DbSet<RecordDbo> Records => Set<RecordDbo>();
    public ApplicationContext() => Database.EnsureCreated();

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseSqlite("Data Source=sqlite.db");
        //  .LogTo(Console.WriteLine);
    }

    protected override void OnModelCreating(ModelBuilder builder)
    {
        builder.Entity<UserDbo>()
            .HasMany(e => e.PublicationDbos)
            .WithOne(e => e.UserDbo)
            .IsRequired();
        
        builder.Entity<UserDbo>()
            .HasMany(e => e.PhotoSessionDbos)
            .WithOne(e => e.UserDbo)
            .IsRequired();

        builder.Entity<PublicationDbo>().Property(e => e.ImagesId)
            .HasConversion(
                v => JsonSerializer.Serialize(v, (JsonSerializerOptions) null!),
                v => JsonSerializer.Deserialize<List<string>>(v, (JsonSerializerOptions) null!)!);
    }
}