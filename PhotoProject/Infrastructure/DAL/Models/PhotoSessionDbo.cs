﻿using Infrastructure.EFCore.Dbo;

namespace PhotoProject.Infrastructure.DAL.Models;

public class PhotoSessionDbo : IEntity<Guid>
{
    public Guid Id { get; set; }
    public string Name { get; set; }
    public string Preview { get; set; }
    public int Price { get; set; }
    public DateTime StartDate { get; set; }
    public DateTime EndDate { get; set; }
    public Status Status { get; set; }
    public string Address { get; set; }
    public UserDbo UserDbo { get; set; }
    public Guid UserDboId { get; set; }
}

public enum Status
{
    Open,
    Closed
}