﻿using Infrastructure.EFCore.Dbo;

namespace PhotoProject.Infrastructure.DAL.Models;

public class PublicationDbo : IEntity<Guid>
{
    public Guid Id { get; set; }
    public string Header { get; set; }
    public string? Text { get; set; }
    public string Preview { get; set; }
    public List<string> ImagesId { get; set; }
    public UserDbo UserDbo { get; set; }
    public Guid UserDboId { get; set; }
    public DateTime DateTime { get; set; }
}