﻿using Infrastructure.EFCore.Dbo;
using Microsoft.EntityFrameworkCore;
using PhotoProject.Infrastructure.DAL.Repositories;

// ReSharper disable ClassNeverInstantiated.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global
// ReSharper disable PropertyCanBeMadeInitOnly.Global
#pragma warning disable CS8618
namespace PhotoProject.Infrastructure.DAL.Models;

public class UserDbo : IEntity<Guid>
{
    public Guid Id { get; set; }
    public string Login { get; set; }
    public string Email { get; set; }
    public string ProfileName { get; set; }
    public string PasswordHash { get; set; }
    public UserExtraFields ExtraFields { get; set; }
    public ICollection<PublicationDbo> PublicationDbos { get; set; }
    public ICollection<PhotoSessionDbo> PhotoSessionDbos { get; set; }
}

[Owned]
public class UserExtraFields
{
    public string? Name { get; set; }
    public string? Surname { get; set; }
    public string? Photo { get; set; }
}