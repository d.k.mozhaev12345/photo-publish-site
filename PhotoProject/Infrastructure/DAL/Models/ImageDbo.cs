﻿using Infrastructure.EFCore.Dbo;

namespace PhotoProject.Infrastructure.DAL.Models;

public class ImageDbo : IEntity<Guid>
{
    public Guid Id { get; set; }
    public byte[] Image { get; set; }
}