﻿using Infrastructure.EFCore.Dbo;

namespace PhotoProject.Infrastructure.DAL.Models;

public class RecordDbo : IEntity<Guid>
{
    public Guid Id { get; set; }
    public Status Status { get; set; }
    public string Email { get; set; }
    public PhotoSessionDbo PhotoSessionDbo { get; set; }
    public Guid PhotoSessionDboId { get; set; }
}