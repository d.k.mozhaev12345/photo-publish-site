namespace PhotoProject.Model;

public class PublicationPreviewModel
{
    public PublicationPreviewModel(string authorLogin, string authorName, string authorSurname, Guid id,
        string header, string text, string preview, List<string> images, string authorProfileName, DateTime dateTime)
    {
        Id = id;
        Header = header;
        Text = text;
        Preview = preview;
        Images = images;
        AuthorProfileName = authorProfileName;
        AuthorLogin = authorLogin;
        AuthorName = authorName;
        AuthorSurname = authorSurname;
        DateTime = dateTime;
    }

    public string AuthorLogin { get; set; }
    public string AuthorName { get; set; }
    public string AuthorSurname { get; set; }
    
    public string AuthorProfileName { get; set; }
    public Guid Id { get; set; }
    public string Header { get; set; }
    public string Text { get; set; }
    public string Preview { get; set; }
    public List<string> Images { get; set; }
    public DateTime DateTime { get; set; }
}