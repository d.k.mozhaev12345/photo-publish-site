﻿using PhotoProject.Infrastructure.DAL.Models;

namespace PhotoProject.Model;

public class RecordPreviewModel
{
    public RecordPreviewModel(Guid id, Guid photoSessionId, string email, Status status)
    {
        Id = id;
        PhotoSessionId = photoSessionId;
        Email = email;
        Status = status;
    }
    
    public Guid Id { get; set; }
    public string Email { get; set; }
    public Guid PhotoSessionId { get; set; }
    public Status Status { get; set; }
}