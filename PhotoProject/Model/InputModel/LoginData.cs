﻿using System.ComponentModel.DataAnnotations;

namespace PhotoProject.Model.InputModel;

public class LoginData
{
    [Required] public string? Login { get; set; }
    [Required] public string? Password { get; set; }
}