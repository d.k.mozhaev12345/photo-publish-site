﻿using PhotoProject.Infrastructure.DAL.Models;

namespace PhotoProject.Model;

public class PhotoSessionPreviewModel
{
    public PhotoSessionPreviewModel(string authorLogin, string authorName, string authorSurname, string authorProfileName, Guid id,
        string name, string preview, int price, DateTime startDate, DateTime endDate, Status status, string address)
    {
        Id = id;
        Name = name;
        Preview = preview;
        Price = price;
        StartDate = startDate;
        EndDate = endDate;
        Status = status;
        Address = address;
        AuthorProfileName = authorProfileName;
        AuthorLogin = authorLogin;
        AuthorName = authorName;
        AuthorSurname = authorSurname;
    }
    
    public string AuthorLogin { get; set; }
    public string AuthorName { get; set; }
    public string AuthorSurname { get; set; }
    public string AuthorProfileName { get; set; }
    public Guid Id { get; set; }
    public string Name { get; set; }
    public string Preview { get; set; }
    public int Price { get; set; }
    public DateTime StartDate { get; set; }
    public DateTime EndDate { get; set; }
    public Status Status { get; set; }
    public string Address { get; set; }
}