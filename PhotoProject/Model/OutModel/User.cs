using PhotoProject.Infrastructure.DAL.Models;

namespace PhotoProject.Model.OutModel;

public class User
{
    public User(string login, string email, UserExtraFields extraFields, string profileName)
    {
        Login = login;
        Email = email;
        ExtraFields = extraFields;
        ProfileName = profileName;
    }
    
    public string Login { get; set; }
    public string Email { get; set; }
    
    public string ProfileName { get; set; }
    public UserExtraFields ExtraFields { get; set; }
}