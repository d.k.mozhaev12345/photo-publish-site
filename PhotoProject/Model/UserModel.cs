﻿using PhotoProject.Infrastructure.DAL.Models;

namespace PhotoProject.Model;

/// <summary>
/// 
/// </summary>
public class UserModel
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="id"></param>
    /// <param name="login"></param>
    /// <param name="name"></param>
    /// <param name="surname"></param>
    /// <param name="profileName"></param>
    public UserModel(Guid id, string login, string name, string surname, string profileName)
    {
        Id = id;
        Login = login;
        Name = name;
        Surname = surname;
        ProfileName = profileName;
    }

    /// <summary>
    /// Конструктор из dbo
    /// </summary>
    /// <param name="user"></param>
    public UserModel(UserDbo user)
    {
        Id = user.Id;
        Login = user.Login;
        Name = user.ExtraFields.Name;
        Surname = user.ExtraFields.Surname;
        ProfileName = user.ProfileName;
    }
    
    public Guid Id { get; set; }

    public string Login { get; }
    public string Name { get; }
    public string Surname { get; }
    
    public string ProfileName { get; }
}