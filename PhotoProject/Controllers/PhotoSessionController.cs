﻿using System.Net;
using Infrastructure.EFCore.DIExtension;
using Microsoft.AspNetCore.Mvc;
using PhotoProject.Infrastructure.DAL.Models;
using PhotoProject.Infrastructure.DAL.Repositories;
using PhotoProject.Model;

namespace PhotoProject.Controllers;

[ApiController]
[Route("[controller]")]
public class PhotoSessionController : BaseController
{
    private readonly PhotoSessionStorage _photoSessionStorage;
    private readonly ImageStorage _imageStorage;
    private readonly RecordStorage _recordStorage;

    public PhotoSessionController(PhotoSessionStorage photoSessionStorage, UserStorage userStorage,
        ImageStorage imageStorage, RecordStorage recordStorage)
    {
        _photoSessionStorage = photoSessionStorage;
        _imageStorage = imageStorage;
        _recordStorage = recordStorage;
    }

    [HttpGet("~/photosessions/{id}")]
    public async Task<IActionResult> GetPhotoSession(Guid id)
    {
        var photoSession = await _photoSessionStorage.GetPhotoSession(id) ?? null;
        if (photoSession is null)
            return StatusCode(StatusCodes.Status404NotFound, "PhotoSession not found");

        return Ok(new PhotoSessionPreviewModel(
            photoSession.UserDbo.Login, photoSession.UserDbo.ExtraFields.Name, photoSession.UserDbo.ExtraFields.Surname,
            photoSession.UserDbo.ProfileName,
            photoSession.Id, photoSession.Name, photoSession.Preview, photoSession.Price, photoSession.StartDate, photoSession.EndDate,
            photoSession.Status, photoSession.Address));
    }

    [HttpDelete("~/photosessions/{id}")]
    public async Task<IActionResult> DeletePhotoSession(Guid id)
    {
        var ownerId = GetUserIdOrNull();
        if (ownerId is null)
            return StatusCode(StatusCodes.Status403Forbidden, "You are not logged in");
        var photoSession = _photoSessionStorage.GetPhotoSession(id).Result;
        if (photoSession is null)
            return StatusCode(StatusCodes.Status404NotFound, "PhotoSession not found");
        var authorId = photoSession.UserDboId;
        if (authorId != ownerId)
            return StatusCode(StatusCodes.Status403Forbidden, "You are not the author of the photoSession");
        await _photoSessionStorage.DeleteAsync(id);
        return StatusCode(StatusCodes.Status204NoContent, "PhotoSession was successfully deleted");
    }

    [HttpPut("~/photosessions/{id}")]
    public async Task<IActionResult> UpdatePhotoSession(Guid id, [FromForm] string name, [FromForm] DateTime startDate,
        [FromForm] DateTime endDate, [FromForm] string address, [FromForm] int price, [FromForm] Status status, IFormFile previewFile)
    {
        var ownerId = GetUserIdOrNull();
        if (ownerId is null)
            return StatusCode(StatusCodes.Status403Forbidden, "You are not logged in");
        var photoSession = _photoSessionStorage.GetPhotoSession(id).Result;
        if (photoSession is null)
            return StatusCode(StatusCodes.Status404NotFound, "PhotoSession not found");
        var authorId = photoSession.UserDboId;
        if (authorId != ownerId)
            return StatusCode(StatusCodes.Status403Forbidden, "You are not the author of the photoSession");

        var previewId = await CreateImage(previewFile);

        var updatedPhotoSession = new PhotoSessionDbo
        {
            Name = name,
            StartDate = startDate,
            EndDate = endDate,
            Address = address,
            Price = price,
            Status = status,
            Preview = previewId.ToString(),
            UserDboId = (Guid)ownerId,
        };

        await _photoSessionStorage.UpdateAsync(updatedPhotoSession);

        return Ok(updatedPhotoSession.Id);
    }

    [HttpPost("~/photosessions")]
    public async Task<IActionResult> CreatePhotoSession([FromForm] string name, [FromForm] DateTime startDate,
        [FromForm] DateTime endDate, [FromForm] string address, [FromForm] int price, [FromForm] Status status, IFormFile previewFile)
    {
        var ownerId = GetUserIdOrNull();
        if (ownerId is null)
            return StatusCode(StatusCodes.Status403Forbidden, "You are not logged in");

        var previewId = await CreateImage(previewFile);
        var photoSession = new PhotoSessionDbo
        {
            Name = name,
            StartDate = startDate,
            EndDate = endDate,
            Address = address,
            Price = price,
            Status = status,
            Preview = previewId.ToString(),
            UserDboId = (Guid)ownerId,
        };

        await _photoSessionStorage.CreateAsync(photoSession);

        return Ok(photoSession.Id);
    }

    [HttpGet("~/photosessions")]
    public async Task<IActionResult> GetAllPhotoSessions()
    {
        var photoSessions = await _photoSessionStorage.GetAllPhotoSessions();
        
        return Ok(ToPhotoSessionPreviewModel(photoSessions));
    }

    [HttpGet("~/photosessions/open")]
    public async Task<IActionResult> GetOpenPhotoSessions()
    {
        var photoSessions = await _photoSessionStorage.GetOpenPhotoSessions();
        
        return Ok(ToPhotoSessionPreviewModel(photoSessions));
    }
    
    [HttpGet("~/photosessions/closed")]
    public async Task<IActionResult> GetClosedPhotoSessions()
    {
        var photoSessions = await _photoSessionStorage.GetClosedPhotoSessions();
        
        return Ok(ToPhotoSessionPreviewModel(photoSessions));
    }

    [HttpGet("~/photosessions-preview-list/{page}")]
    public async Task<IActionResult> PhotoSessionsPreviewList(int page = 1)
    {
        var photoSessions = await _photoSessionStorage.GetPhotoSessionsByPage(page);

        return Ok(ToPhotoSessionPreviewModel(photoSessions));
    }
    
    [HttpPost("~/photosessions/{id}/records")]
    public async Task<IActionResult> CreateRecord(Guid id, [FromForm] string email)
    {
        var photoSession = await _photoSessionStorage.GetPhotoSession(id) ?? null;
        if (photoSession is null)
            return StatusCode(StatusCodes.Status404NotFound, "PhotoSession not found");
        if (photoSession.Status == Status.Closed)
            return StatusCode(StatusCodes.Status418ImATeapot, "Record is closed"); 
        var record = new RecordDbo
        {
            Status = Status.Open,
            Email = email,
            PhotoSessionDboId = photoSession.Id
        };

        await _recordStorage.CreateAsync(record);

        return Ok(record.Id);
    }
    
    [HttpGet("~/photosessions/{id}/records/{recordId}")]
    public async Task<IActionResult> GetRecord(Guid id, Guid recordId)
    {
        var photoSession = await _photoSessionStorage.GetPhotoSession(id) ?? null;
        if (photoSession is null)
            return StatusCode(StatusCodes.Status404NotFound, "PhotoSession not found");
        var record = await _recordStorage.GetRecord(recordId) ?? null;
        if (record is null)
            return StatusCode(StatusCodes.Status404NotFound, "Record not found");

        return Ok(new RecordPreviewModel(record.Id, record.PhotoSessionDboId, record.Email, record.Status));
    }

    [HttpPut("~/photosessions/{id}/records/{recordId}")]
    public async Task<IActionResult> UpdateRecord(Guid id, Guid recordId, [FromForm] Status status)
    {
        var ownerId = GetUserIdOrNull();
        if (ownerId is null)
            return StatusCode(StatusCodes.Status403Forbidden, "You are not logged in");
        var photoSession = _photoSessionStorage.GetPhotoSession(id).Result;
        if (photoSession is null)
            return StatusCode(StatusCodes.Status404NotFound, "PhotoSession not found");
        var authorId = photoSession.UserDboId;
        if (authorId != ownerId)
            return StatusCode(StatusCodes.Status403Forbidden, "You are not the author of the photoSession");
        var record = _recordStorage.GetRecord(recordId).Result;
        if (record is null)
            return StatusCode(StatusCodes.Status404NotFound, "Record not found");
        if (status == Status.Open)
            return StatusCode(StatusCodes.Status403Forbidden, "You can not change status to Open");
        
        var updatedRecord = new RecordDbo
        {
            Id = record.Id,
            PhotoSessionDboId = record.PhotoSessionDboId,
            Email = record.Email,
            Status = status
        };
        
        await _recordStorage.DeleteAllExceptOne(photoSession.Id, updatedRecord.Id);
        await _recordStorage.DeleteAsync(record.Id);
        await _recordStorage.CreateAsync(updatedRecord);

        var updatedPhotoSession = new PhotoSessionDbo
        {
            Id = photoSession.Id,
            Name = photoSession.Name,
            StartDate = photoSession.StartDate,
            EndDate = photoSession.EndDate,
            Address = photoSession.Address,
            Price = photoSession.Price,
            Status = status,
            Preview = photoSession.Preview,
            UserDboId = (Guid)ownerId,
        };
        await _photoSessionStorage.DeleteAsync(photoSession.Id);
        await _photoSessionStorage.CreateAsync(updatedPhotoSession);
        await _photoSessionStorage.SaveAsync();
        await _recordStorage.SaveAsync();
        return Ok(updatedRecord.Id);
    }
    
    [HttpGet("~/photosessions/{id}/records")]
    public async Task<IActionResult> GetRecords(Guid id)
    {
        var records = await _recordStorage.GetRecordByPhotoSession(id);

        return Ok(ToRecordPreviewModel(records));
    }

    private async Task<Guid> CreateImage(IFormFile file)
    {
        await using var fileStream = file.OpenReadStream();
        var bytesImage = new byte[file.Length];
        await fileStream.ReadAsync(bytesImage, 0, (int)file.Length);
        fileStream.Close();
        var image = new ImageDbo { Image = bytesImage };
        var imageId = await _imageStorage.CreateAsync(image);
        return imageId;
    }

    private static IEnumerable<PhotoSessionPreviewModel> ToPhotoSessionPreviewModel(
        IEnumerable<PhotoSessionDbo> photoSessions) =>
        photoSessions
            .Select(x => new PhotoSessionPreviewModel(
                x.UserDbo.Login, x.UserDbo.ExtraFields.Name, x.UserDbo.ExtraFields.Surname, x.UserDbo.ProfileName,
                x.Id, x.Name, x.Preview, x.Price, x.StartDate, x.EndDate, x.Status, x.Address));

    private static IEnumerable<RecordPreviewModel> ToRecordPreviewModel(
        IEnumerable<RecordDbo> photoSessions) =>
        photoSessions
            .Select(x => new RecordPreviewModel(x.Id, x.PhotoSessionDboId, x.Email, x.Status));
}