using System.ComponentModel.DataAnnotations;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Net.Http.Headers;
using PhotoProject.Infrastructure.Auth;
using PhotoProject.Infrastructure.DAL.Models;
using PhotoProject.Infrastructure.DAL.Repositories;
using PhotoProject.Model.InputModel;
using PhotoProject.Models.OutModel;

namespace PhotoProject.Controllers;

[Route("/auth")]
public class AuthController : Controller
{
    private readonly UserStorage _userStorage;
    private readonly IJwtAuthManager _jwtAuthManager;
    private readonly ImageStorage _imageStorage;

    public AuthController(UserStorage userStorage, IJwtAuthManager jwtAuthManager, ImageStorage imageStorage)
    {
        _userStorage = userStorage;
        _jwtAuthManager = jwtAuthManager;
        _imageStorage = imageStorage;
    }

    [AllowAnonymous]
    [HttpPost("/login")]
    public async Task<IActionResult> Login([FromBody] LoginData loginData)
    {
        var login = loginData.Login;
        var password = loginData.Password;

        if (string.IsNullOrEmpty(login) || string.IsNullOrEmpty(password))
            return Forbid();

        var user = await _userStorage.GetByLogin(login);
        if (user == default) return Forbid();

        var passwordValidator = new PasswordHasher<UserDbo>();
        var isPasswordValid = passwordValidator.VerifyHashedPassword(null!, user.PasswordHash, password);
        if (isPasswordValid != PasswordVerificationResult.Failed)
        {
            var claims = new List<Claim>
            {
                new(ClaimTypes.Name, user.Login),
                new(ClaimTypes.NameIdentifier, user.Id.ToString())
            };
            var roles = new List<Claim>();

            var jwtResult = _jwtAuthManager.GenerateTokens(login, claims.ToArray(), DateTime.UtcNow);
            if (jwtResult is not null)
                return Ok(new LoginResult
                {
                    Login = login,
                    Roles = roles.Select(x => x.Value).ToArray(),
                    AccessToken = jwtResult.AccessToken,
                    RefreshToken = jwtResult.RefreshToken.TokenString
                });
        }

        return Forbid();
    }

    [Authorize]
    [HttpPost("/logout")]
    public Task<IActionResult> Logout()
    {
        var userName = User.Identity?.Name ?? throw new InvalidOperationException();
        _jwtAuthManager.RemoveRefreshTokenByUserName(userName);
        return Task.FromResult<IActionResult>(Ok());
    }

    [AllowAnonymous]
    [HttpPost("/register")]
    public async Task<IActionResult> Register([FromForm] [Required] string login, [FromForm] [Required] string password,
        [FromForm] [Required] string email, [FromForm] [Required] string profileName, [FromForm] string? name,
        [FromForm] string? surName, IFormFile photo)
    {
        email = email.ToLowerInvariant();

        var user = (await _userStorage.ReadAsync(x => x.Login == login || x.Email == email)).FirstOrDefault();
        if (user != default)
            return Forbid();

        var imageId = await CreateImage(photo);
        user = new UserDbo
        {
            Login = login,
            Email = email,
            ProfileName = profileName,
            ExtraFields = new UserExtraFields
            {
                Name = name,
                Surname = surName,
                Photo = imageId.ToString()
            },
        };

        var passwordHasher = new PasswordHasher<UserDbo>();
        user.PasswordHash = passwordHasher.HashPassword(null!, password);
        await _userStorage.CreateAsync(user);
        return Ok();
    }

    [HttpPost("/refresh-token")]
    public async Task<ActionResult> RefreshToken([Required] string refreshToken)
    {
        try
        {
            var login = User.Identity?.Name;
            if (string.IsNullOrWhiteSpace(refreshToken)) return Unauthorized();

            var accessToken = Request.Headers[HeaderNames.Authorization].ToString()[7..];
            var jwtResult = await _jwtAuthManager.Refresh(refreshToken, accessToken, DateTime.UtcNow);
            return Ok(new LoginResult
            {
                Login = jwtResult.RefreshToken.Login,
                Roles = User.FindAll(ClaimTypes.Role).Select(x => x.Value).ToArray(),
                AccessToken = jwtResult.AccessToken,
                RefreshToken = jwtResult.RefreshToken.TokenString
            });
        }
        catch (SecurityTokenException e)
        {
            return Unauthorized(e.Message); // return 401 so that the client side can redirect the user to login page
        }
    }
    
    private async Task<Guid?> CreateImage(IFormFile file)
    {
        if (file is null)
            return null;
        await using var fileStream = file.OpenReadStream();
        var bytesImage = new byte[file.Length];
        await fileStream.ReadAsync(bytesImage, 0, (int)file.Length);
        fileStream.Close();
        var image = new ImageDbo { Image = bytesImage };
        var imageId = await _imageStorage.CreateAsync(image);
        return imageId;
    }
}