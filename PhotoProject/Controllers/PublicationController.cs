using System.Net;
using Infrastructure.EFCore.DIExtension;
using Microsoft.AspNetCore.Mvc;
using PhotoProject.Infrastructure.DAL.Models;
using PhotoProject.Infrastructure.DAL.Repositories;
using PhotoProject.Model;

namespace PhotoProject.Controllers;

/// <summary>
/// Публикации
/// </summary>
[ApiController]
[Route("[controller]")]
public class PublicationController : BaseController
{
    private readonly PublicationStorage _publicationStorage;
    private readonly ImageStorage _imageStorage;

    /// <summary>
    /// Публикации
    /// </summary>
    /// <param name="publicationStorage"></param>
    /// <param name="userStorage"></param>
    /// <param name="imageStorage"></param>
    public PublicationController(PublicationStorage publicationStorage, UserStorage userStorage,
        ImageStorage imageStorage)
    {
        _publicationStorage = publicationStorage;
        _imageStorage = imageStorage;
    }


    /// <summary>
    /// Получение публикации по Id
    /// </summary>
    /// <param name="id">Id публикации</param>
    [HttpGet("~/publications/{id}")]
    public async Task<IActionResult> GetPublication(Guid id)
    {
        var publication = await _publicationStorage.GetPublication(id) ?? null;
        if (publication is null)
            return StatusCode(StatusCodes.Status404NotFound, "Publication not found");

        return Ok(new PublicationPreviewModel(
            publication.UserDbo.Login, publication.UserDbo.ExtraFields.Name, publication.UserDbo.ExtraFields.Surname,
            publication.Id, publication.Header, publication.Text, publication.Preview,
            publication.ImagesId, publication.UserDbo.ProfileName, publication.DateTime));
    }

    /// <summary>
    /// Удаление публикации по Id
    /// </summary>
    /// <param name="id">Id публикации</param>
    /// <returns></returns>
    [HttpDelete("~/publications/{id}")]
    public async Task<IActionResult> DeletePublication(Guid id)
    {
        var ownerId = GetUserIdOrNull();
        if (ownerId is null)
            return StatusCode(StatusCodes.Status403Forbidden, "You are not logged in");
        var publication = _publicationStorage.GetPublication(id).Result;
        if (publication is null)
            return StatusCode(StatusCodes.Status404NotFound, "Publication not found");
        var authorId = publication.UserDboId;
        if (authorId != ownerId)
            return StatusCode(StatusCodes.Status403Forbidden, "You are not the author of the publication");
        await _publicationStorage.DeleteAsync(id);
        return StatusCode(StatusCodes.Status204NoContent, "Publication was successfully deleted");
    }

    /// <summary>
    /// Изменить публикацию по Id
    /// </summary>
    /// <param name="header">Заголовок публикации</param>
    /// <param name="text">Текст содержимого публикации</param>
    /// <param name="previewFile">Превью публикации</param>
    /// <param name="files">Список фотографий</param>
    /// <returns>Возвращает Id созданной публикации</returns>
    [HttpPut("~/publications/{id}")]
    public async Task<IActionResult> UpdatePublication(Guid id, [FromForm] string header, [FromForm] string? text,
        IFormFile previewFile, List<IFormFile> files)
    {
        var ownerId = GetUserIdOrNull();
        if (ownerId is null)
            return StatusCode(StatusCodes.Status403Forbidden, "You are not logged in");
        var publication = _publicationStorage.GetPublication(id).Result;
        if (publication is null)
            return StatusCode(StatusCodes.Status404NotFound, "Publication not found");
        var authorId = publication.UserDboId;
        if (authorId != ownerId)
            return StatusCode(StatusCodes.Status403Forbidden, "You are not the author of the publication");

        var previewId = await CreateImage(previewFile);
        var imagesIds = new List<string>();
        foreach (var file in files)
        {
            var imageId = await CreateImage(file);
            imagesIds.Add(imageId.ToString());
        }

        var updatedPublication = new PublicationDbo
        {
            Id = id,
            Header = header,
            Text = text,
            Preview = previewId.ToString(),
            ImagesId = imagesIds,
            UserDboId = (Guid)ownerId,
            DateTime = publication.DateTime
        };

        await _publicationStorage.UpdateAsync(updatedPublication);

        return Ok(updatedPublication.Id);
    }

    /// <summary>
    /// Создание публикации
    /// </summary>
    /// <param name="header">Заголовок публикации</param>
    /// <param name="text">Текст содержимого публикации</param>
    /// <param name="previewFile">Превью публикации</param>
    /// <param name="files">Список фотографий</param>
    /// <returns>Возвращает Id созданной публикации</returns>
    [HttpPost("~/publish")]
    public async Task<IActionResult> Publish([FromForm] string header, [FromForm] string? text, IFormFile previewFile,
        List<IFormFile> files)
    {
        var ownerId = GetUserIdOrNull();
        if (ownerId is null)
            return StatusCode(StatusCodes.Status403Forbidden, "You are not logged in");
        var previewId = await CreateImage(previewFile);
        var imagesIds = new List<string>();
        foreach (var file in files)
        {
            var imageId = await CreateImage(file);
            imagesIds.Add(imageId.ToString());
        }

        var publication = new PublicationDbo
        {
            Header = header,
            Text = text,
            Preview = previewId.ToString(),
            ImagesId = imagesIds,
            UserDboId = (Guid)ownerId,
            DateTime = DateTime.UtcNow
        };

        await _publicationStorage.CreateAsync(publication);

        return Ok(publication.Id);
    }

    /// <summary>
    /// Получение пагинированного списка превью публикаций
    /// </summary>
    /// <param name="id">Номер страницы</param>
    /// <returns></returns>
    [HttpGet("~/publications-preview-list/{page}")]
    public async Task<IActionResult> PublicationsPreviewList(int page = 1)
    {
        var publications = await _publicationStorage.GetPublicationsByPage(page);

        var result = publications
            .Select(x => new PublicationPreviewModel(
                x.UserDbo.Login, x.UserDbo.ExtraFields.Name, x.UserDbo.ExtraFields.Surname,
                x.Id, x.Header, x.Text, x.Preview, x.ImagesId, x.UserDbo.ProfileName, x.DateTime));

        return Ok(result);
    }

    private async Task<Guid> CreateImage(IFormFile file)
    {
        await using var fileStream = file.OpenReadStream();
        var bytesImage = new byte[file.Length];
        await fileStream.ReadAsync(bytesImage, 0, (int)file.Length);
        fileStream.Close();
        var image = new ImageDbo { Image = bytesImage };
        var imageId = await _imageStorage.CreateAsync(image);
        return imageId;
    }
}