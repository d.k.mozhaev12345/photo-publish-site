﻿using Infrastructure.EFCore.DIExtension;
using Microsoft.AspNetCore.Mvc;
using PhotoProject.Infrastructure.DAL.Models;
using PhotoProject.Infrastructure.DAL.Repositories;
using PhotoProject.Model;
using PhotoProject.Model.OutModel;

namespace PhotoProject.Controllers;

/// <summary>
/// Всякая херня для взаимодействия с аккаунтом юзера
/// </summary>
[ApiController]
[Route("users")]
public class UserController : BaseController
{
    private readonly PublicationStorage _publicationStorage;
    private readonly UserStorage _userStorage;
    private readonly PhotoSessionStorage _photoSessionStorage;

    /// <summary>
    /// Пользователи
    /// </summary>
    /// <param name="publicationStorage"></param>
    /// <param name="userStorage"></param>
    public UserController(PublicationStorage publicationStorage, PhotoSessionStorage photoSessionStorage, UserStorage userStorage)
    {
        _userStorage = userStorage;
        _publicationStorage = publicationStorage;
        _photoSessionStorage = photoSessionStorage;
        Console.WriteLine("Создали контроллер");
    }

    /// <summary>
    /// Получение информации о пользователе по логину
    /// </summary>
    /// <param name="login">Логин пользователя</param>
    /// <returns></returns>
    [HttpGet("{login}")]
    public async Task<IActionResult> GetUserByLogin(string login)
    {
        var user = await _userStorage.GetByLogin(login);
        if (user is null)
            return StatusCode(StatusCodes.Status404NotFound, "User not found");

        return Ok(new User(user!.Login, user.Email, user.ExtraFields, user.ProfileName));
    }

    /// <summary>
    /// Получение всех публикаций пользователя
    /// </summary>
    /// <param name="login">Логин пользователя</param>
    /// <returns></returns>
    [HttpGet("{login}/publications")]
    public async Task<IActionResult> GetUserPublications(string login)
    {
        var user = await _userStorage.GetByLogin(login);
        if (user is null)
            return StatusCode(StatusCodes.Status404NotFound, "User not found");

        var publications = await _publicationStorage.GetUserPublications(user!.Id);
        
        return Ok(publications.Select(x => new PublicationPreviewModel(
            x.UserDbo.Login, x.UserDbo.ExtraFields.Name, x.UserDbo.ExtraFields.Surname,
            x.Id, x.Header, x.Text, x.Preview, x.ImagesId, x.UserDbo.ProfileName, x.DateTime)));
    }
    
    [HttpGet("{login}/photosessions")]
    public async Task<IActionResult> GetUserPhotoSessions(string login)
    {
        var user = await _userStorage.GetByLogin(login);
        if (user is null)
            return StatusCode(StatusCodes.Status404NotFound, "User not found");

        var photoSessions = await _photoSessionStorage.GetUserPhotoSessions(user!.Id);
        
        return Ok(ToPreviewModel(photoSessions));
    }
    
    [HttpGet("{login}/photosessions/open")]
    public async Task<IActionResult> GetUserOpenPhotoSessions(string login)
    {
        var user = await _userStorage.GetByLogin(login);
        if (user is null)
            return StatusCode(StatusCodes.Status404NotFound, "User not found");

        var photoSessions = await _photoSessionStorage.GetUserOpenPhotoSessions(user!.Id);
        
        return Ok(ToPreviewModel(photoSessions));
    }
    
    [HttpGet("{login}/photosessions/closed")]
    public async Task<IActionResult> GetUserClosedPhotoSessions(string login)
    {
        var user = await _userStorage.GetByLogin(login);
        if (user is null)
            return StatusCode(StatusCodes.Status404NotFound, "User not found");

        var photoSessions = await _photoSessionStorage.GetUserClosedhotoSessions(user!.Id);
        
        return Ok(ToPreviewModel(photoSessions));
    }
    
    private static IEnumerable<PhotoSessionPreviewModel> ToPreviewModel(
        IEnumerable<PhotoSessionDbo> photoSessions) =>
        photoSessions
            .Select(x => new PhotoSessionPreviewModel(
                x.UserDbo.Login, x.UserDbo.ExtraFields.Name, x.UserDbo.ExtraFields.Surname, x.UserDbo.ProfileName,
                x.Id, x.Name, x.Preview, x.Price, x.StartDate, x.EndDate, x.Status, x.Address));
}