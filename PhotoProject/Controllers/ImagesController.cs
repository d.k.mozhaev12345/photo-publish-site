using System.Net;
using Infrastructure.EFCore.DIExtension;
using Microsoft.AspNetCore.Mvc;
using PhotoProject.Infrastructure.DAL.Models;
using PhotoProject.Infrastructure.DAL.Repositories;

namespace PhotoProject.Controllers;

/// <summary>
/// Картинки
/// </summary>
[ApiController]
[Route("images")]
public class ImagesController : Controller
{
    private readonly ImageStorage _imageStorage;

    /// <summary>
    /// Картинки
    /// </summary>
    /// <param name="imageStorage"></param>
    public ImagesController(ImageStorage imageStorage)
    {
        _imageStorage = imageStorage;
    }

    /// <summary>
    /// Получить картинку по Id
    /// </summary>
    /// <param name="id">Id картинки</param>
    /// <returns></returns>
    [HttpGet("{id}")]
    public async Task<IActionResult> GetImage(Guid id)
    {
        var image = await _imageStorage.ReadAsync(id) ?? null;
        if (image == null) 
            return StatusCode((int)HttpStatusCode.NoContent);
        return File(image.Image, "image/jpeg");
    }

    /// <summary>
    /// Удалить картинку по Id
    /// </summary>
    /// <param name="id">Id картинки</param>
    /// <returns></returns>
    [HttpDelete("{id}")]
    public async Task<IActionResult> DeleteImage(Guid id)
    {
        await _imageStorage.DeleteAsync(id);
        return Ok();
    }
    
    /// <summary>
    /// Изменить картинку по Id
    /// </summary>
    /// <param name="id">Id картинки</param>
    /// <param name="image">Новая картинка</param>
    /// <returns></returns>
    [HttpPut("{id}")]
    public async Task<IActionResult> UpdateImage(Guid id, IFormFile image)
    {
        await using var fileStream = image.OpenReadStream();
        var bytesImage = new byte[image.Length];
        await fileStream.ReadAsync(bytesImage, 0, (int)image.Length);
        fileStream.Close();
        var imageDbo = new ImageDbo { Id = id, Image = bytesImage };
        await _imageStorage.UpdateAsync(imageDbo);
        return File(imageDbo.Image, "image/jpeg");
    }
}