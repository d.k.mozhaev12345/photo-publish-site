﻿using System.Security.Claims;
using Microsoft.AspNetCore.Mvc;

namespace PhotoProject.Controllers;

public abstract class BaseController : Controller
{
    protected Guid? GetUserIdOrNull()
    {
        var userIdString = User.FindFirstValue(ClaimTypes.NameIdentifier);
        if (string.IsNullOrWhiteSpace(userIdString))
            return null;

        return Guid.Parse(userIdString);
    }
}