﻿using System.Linq.Expressions;

namespace Infrastructure.EFCore.Storage;

public interface IStorage<T, in TKey> : IDisposable
{
    IEnumerable<T> ReadAll();
    Task<IEnumerable<T>> ReadAllASync();
    Guid Create(T item);
    Task<Guid> CreateAsync(T item);
    T? Read(TKey id);
    Task<T?> ReadAsync(TKey id);
    IEnumerable<T> Read(Expression<Func<T, bool>> lambda);
    Task<IEnumerable<T>> ReadAsync(Expression<Func<T, bool>> lambda);
    void Update(T item);
    Task UpdateAsync(T item);
    void Delete(TKey id);
    Task DeleteAsync(TKey id);
    void Save();
    Task SaveAsync();
}