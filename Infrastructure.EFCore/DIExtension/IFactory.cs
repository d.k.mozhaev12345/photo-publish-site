namespace Infrastructure.EFCore.DIExtension;

public interface IFactory<out T>
{
    public T Create();
}