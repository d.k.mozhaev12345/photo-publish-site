﻿namespace Infrastructure.EFCore.Dbo;

public interface IEntity<TId>
{
    public TId Id { get; set; }
}