# Photo Publish Site

## Screenshots

![image info](screenshots/Screenshot_from_2023-05-15_01-26-17.png)
![image info](screenshots/Screenshot_from_2023-05-15_01-29-38.png)

## Getting started

### Backend
Start from Rider

### Frontend
```
cd Frontend
npm install
npm run start-dev
```
Go to `http://localhost:9000`
